//Node
import React, {Component} from'react';
import { Panel, PageHeader, Jumbotron, Collapse } from 'react-bootstrap'
//Components
//Styles
import './feed-comp.css';

export default class Feed extends Component {
  state = {
    open: false,
		...this.props
	}

  componentDidMount = () => {
    let w = document.getElementById(this.state.id).clientWidth
    console.log(`height: ${((w*9)/16)}`)
    this.setState({ height: ((w*9)/16)})
  }

  //TODO: Complete Order Sequence
  imageOrder = (evt) => {
    console.log(evt.target.getAttribute('index'))
    let main = evt.target.getAttribute('index')
    this.setState({
      images: [
        ...this.state.images.slice((main-1), (this.state.images.length)),
        ...this.state.images.slice(0, main-1)
      ]
    })
  }

  render() {
    let index = 0
    return (
      <Panel id={this.state.id}>
        <h3>{this.state.title}</h3>
        <PageHeader></PageHeader>
        <Jumbotron className="pad"><h4>{this.state.desc}</h4></Jumbotron>
        <div className='image-box'>
          { this.state.images.map(img=> {
            index++
            return (<div
              key={index}
              index={index}
              id={`${this.state.id}-${index}`}
              className='image'
              style={{
                backgroundImage: `url(${img})`,
                height: index === 1 && this.state.height
              }}
              onClick={clk => this.imageOrder(clk)}
            />)
          })}
        </div>
        <PageHeader></PageHeader>
        <Collapse in={this.state.open} className='show-little' style={{height: '0px'}}>
          <div>
            <h4 className='content'>{`${this.state.content}`}</h4>
          </div>
        </Collapse>
        <strong onClick={() => this.setState({ open: !this.state.open })}>
          <h4 style={{fontWeight: "bold"}}>{
            this.state.open ? `Show Less` :  `Read More...`
          }</h4>
        </strong>
      </Panel>
    )
  }
}
