(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["service"] = service;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_node_fetch__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_node_fetch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_node_fetch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rss_parser__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rss_parser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rss_parser__);



function service(evt, ctx, cb) {

  const jsondata = __WEBPACK_IMPORTED_MODULE_0_node_fetch___default()('http://www.ladbible.com/news.rss',  {
  	method: 'get',
    mode: 'cors'
  }).then(resp => {
    return resp.text()
  }).then(text => {
    const images = text.split('<item>').map(img => {
      return img.split('"').filter(i => {
        return i.includes("http://20.theladbiblegroup.com/s3/content/")
      })
    }).slice(1)
    return new Promise((res, rej) => {
      __WEBPACK_IMPORTED_MODULE_1_rss_parser___default.a.parseString(text, (err, parsed) => {
        if(err !== null) rej(err)
        res(parsed)
      })
    }).then(json => {
      let entries = json.feed.entries
      for(let entry in entries){
        entries[entry][`images`] = images[entry]
        entries[entry][`content`] = entries[entry][`content:encoded`]
          .replace(/(<br>|<\/p>)/g, '\n')
          .replace(/(<\/?[^>]+(>|$)|\t)/g, '')
          .replace(/\.(\S)/g, '. $1')
          .replace(/\\"/g, '"');
        delete entries[entry]['content:encoded']
        delete entries[entry]['pubDate']
        delete entries[entry]['guid']
      }
      return json
    }).catch(err => {
      console.error(err)
    })
  }).then(data => {
    ctx.done(null, data)
  }).catch(err => {
    console.error(err)
  })
}


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("node-fetch");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("rss-parser");

/***/ })
/******/ ])));