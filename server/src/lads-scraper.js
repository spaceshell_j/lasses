import fetch from 'node-fetch';
import parser from 'rss-parser'

export function service(evt, ctx, cb) {

  const jsondata = fetch('http://www.ladbible.com/news.rss',  {
  	method: 'get',
    mode: 'cors'
  }).then(resp => {
    return resp.text()
  }).then(text => {
    const images = text.split('<item>').map(img => {
      return img.split('"').filter(i => {
        return i.includes("http://20.theladbiblegroup.com/s3/content/")
      })
    }).slice(1)
    return new Promise((res, rej) => {
      parser.parseString(text, (err, parsed) => {
        if(err !== null) rej(err)
        res(parsed)
      })
    }).then(json => {
      let entries = json.feed.entries
      for(let entry in entries){
        entries[entry][`images`] = images[entry]
        entries[entry][`content`] = entries[entry][`content:encoded`]
          .replace(/(<br>|<\/p>)/g, '\n')
          .replace(/(<\/?[^>]+(>|$)|\t)/g, '')
          .replace(/\.(\S)/g, '. $1')
          .replace(/\\"/g, '"');
        delete entries[entry]['content:encoded']
        delete entries[entry]['pubDate']
        delete entries[entry]['guid']
      }
      return json
    }).catch(err => {
      console.error(err)
    })
  }).then(data => {
    ctx.done(null, data)
  }).catch(err => {
    console.error(err)
  })
}
